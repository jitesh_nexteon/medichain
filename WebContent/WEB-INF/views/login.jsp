<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
    <title>Profile Dashboard</title>
    <link rel="stylesheet" href="static/css/vendor.css" type="text/css" media="all">
    <link rel="stylesheet" href="static/css/main.css" type="text/css" media="all" />
</head>

<body>
    <div class="login-panel-wrap">
        <div class="login-panel">
            <div class="login-center">
                <div class="login-wrap">
                    <div class="login-hd">
                        <h3>Login Screen</h3>
                    </div>
                    <form action="login" method="post">
                        <div class="form-group">
                            <label for="exampleInputEmail1">User Login</label>
                            <input type="text"  class="form-control" placeholder="Bhamashah Id or Email" name="username" required/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                             <input type="password"  class="form-control" placeholder="Password" name="password"/ required>
                        </div>
                        <div class="form-group">
                            
                        </div>
                        <div class="form-group">
                            New User, Register </br><a href="register" class="fp-text">Here</a>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>
                        
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				    
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="build/js/vendor-min.js"></script>
    <script type="text/javascript" src="build/js/main.js"></script>
</body>

</html>