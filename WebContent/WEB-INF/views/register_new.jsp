<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
    <title>Profile Dashboard</title>
    <link rel="stylesheet" href="static/css/vendor.css" type="text/css" media="all">
    <link rel="stylesheet" href="static/css/main.css" type="text/css" media="all" />
</head>

<body>
    <div class="login-panel-wrap">
        <div class="login-panel">
            <div class="login-center">
                <div class="login-wrap">
                    <div class="login-hd">
                        <h3>Registration Screen</h3>
                    </div>
                    <form class="form-group" action="register" method="post">
                        <div class="form-group">
                            <label for="InputName">Name</label>
                            <input type="text" class="form-control" placeholder="Name" name="name" required/>
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Emal Id or Bhamashah Id</label>
                            <input type="text" class="form-control" placeholder="Bhamashah Id or Email" name="family" required/>
                        </div>
                        
                        <div class="form-group">
                            <label for="InputPassword">Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="pass" required/>
                        </div>
                        <div class="form-group">
                            <label for="InputConfirmPassword">Confirm Password</label>
                             <input type="password"  class="form-control" placeholder="Confirm Password" name="cpass"/>
                        </div>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">
                            Already a user Sign In<div><a href="login" class="fp-text">Sign-In</a></div>
                            <button type="submit" class="btn btn-primary">Register Now</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="static/js/vendor-min.js"></script>
    <script type="text/javascript" src="static/js/main.js"></script>
</body>

</html>>