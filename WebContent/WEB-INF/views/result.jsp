<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"  %>   
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
    <title>File Upload result</title>
    <link rel="stylesheet" href="static/css/vendor.css" type="text/css" media="all">
    <link rel="stylesheet" href="static/css/main.css" type="text/css" media="all" />
</head>

<body>
 <div class="upload-wrap">
   
    <!-- top navigation -->
    <%@ include file = "/WEB-INF/views/top-navigation.jsp" %>	
</div>
    <div class="container">
        <div class="user-detail-wrap">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <figure class="user-img">
                                <img src="static/images/member.jpg" alt="">
                            </figure>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <ul class="user-det-colum">
                                <li>
                                    <small>Name</small>
                                    <span>${userData.name}</span>
                                </li>
                                <li>
                                    <small>Email</small>
                                    <span>${ userData.username }</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-6 col-lg-6">
                    <h4>Bhamashah Id</h4>
                    <p>${ userData.username }</p>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <a href="#" class="btn btn-primary btn-xs">Share This Profile</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="doclist-view">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Document Title</th>
                                    <th>Type</th>
                                    <th>Transaction Id</th>
                                    <th>Uploaded By</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="8">
                                        <div class="allow-top">
                                            <h4>01 March 2017</h4>
                                            <div class="allow-to-access">
                                                <input type="checkbox" name="" id="a1">
                                                <label for="a1">Allow Access</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <core:forEach items="${ files }" var="files">
                                <tr>
                                    <th scope="row">1</th>
                                    <td><core:out value="${fn:substringAfter( files.filePath ,'.') }"/></td>
                                    <td>
                                        <a href="${ files.filePath }" class="link-icon"></a>
                                    </td>
                                    <td>${ files.txid }</td>
                                    <td>${ files.filePath }</td>
                                    <td><a href="#" class="btn-trash"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                </tr>
                                </core:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="static/js/vendor-min.js"></script>
    <script type="text/javascript" src="static/js/main.js"></script>
</body>

</html>