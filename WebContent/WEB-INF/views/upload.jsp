
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="static/favicon.ico" />
    <title>Profile Dashboard</title>
    <link rel="stylesheet" href="static/css/vendor.css" type="text/css" media="all" />
    <link rel="stylesheet" href="static/css/main.css" type="text/css" media="all">
</head>

<body>
    <div class="upload-wrap">
    <div>
    <!-- top navigation -->
    <%@ include file = "/WEB-INF/views/top-navigation.jsp" %>	
		</div>
        <div class="upload-section">
            <form action="FileUpload" method="post"  enctype="multipart/form-data"  name="productForm" id="productForm"">
                <div class="form-group">
                <label for="exampleInputEmail1">Disease</label>
                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Write about your disease" required>
            </div>
            <div class="upload-hd">
                <h3>Upload Documents</h3>
                 <a href="profile" class="view-list">View Documents List</a> 
            </div>            
            <div class="upload-colum">                
                <div class="upload-row">
                    <div class="file-input">
                        <div class="input-group">
                            <input type="text" class="form-control" value="Browse and Upload" readonly>
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Browse&hellip; <input type="file" name="file" id="file" class="inputFile" style="display: none;">
                                </span>
                            </label>
                        </div>
                        <span class="alert alert-danger">Error Message</span>
                    </div>
                    <div class="file-add-btn">
                        <a href="#" class="add-element"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>                    
                    </div>                
                </div>                
            </div>
            <div class="upload-bottom">
                <p>Upload max file size, Format</p>                
                <button type="submit" class="btn btn-primary" id="uploadFile">Upload & Save</button>
            </div>
            </form>
        </div>
    </div>
    </div>
    <script src="static/js/vendor-min.js"></script>
    <script type="text/javascript" src="static/js/main.js"></script>
</body>

</html>