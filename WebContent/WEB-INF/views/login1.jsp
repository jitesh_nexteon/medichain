<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"  %>   
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="/static/favicon.ico" />
    <title>Login Or Register</title>
    <link rel="stylesheet" href="/static/css/vendor.css" type="text/css" media="all">
    <link rel="stylesheet" href="/static/css/main.css" type="text/css" media="all" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<div class="container">
  <div class="info">
    <h1>Login</h1>
     		<core:if test="${ not empty error }">
            	<div class="alert alert-success">
            		${ error }
            	</div>
            </core:if>
  </div>
</div>


<div class="container">
				<h1>Login to Your Account</h1><br>
				  <form class="form-group" action="login" method="post">
				    <input type="text"  class="form-control" placeholder="Bhamashah Id or Email" name="username"/>
				    <input type="password"  class="form-control" placeholder="Password" name="password"/>
				    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				    <button type="submit" class="btn btn-info">Login </button>
				  </form>
</div>

<div class="container">
					<h1>Register to Your Account</h1><br>
				    <form class="form-group" action="register" method="post">
					    <input type="text" class="form-control" placeholder="Bhamashah Id or Email" name="family"/>
					    <input type="text" class="form-control" placeholder="Name" name="name"/>
					    <input type="password" class="form-control" placeholder="Password" name="pass"/>
					    <input type="password"  class="form-control" placeholder="Confirm Password" name="cpass"/>
					    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					    <button type="submit" class="btn btn-info">Register </button>
					  </form>
</div>

</body>
</html>