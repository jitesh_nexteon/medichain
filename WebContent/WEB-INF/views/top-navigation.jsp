<div class="top_nav">
	<div>


		<ul class="nav navbar-nav navbar-right">
			<li class="">User : ${pageContext.request.userPrincipal.name}</li>

			<li>
				<form action="logout" method="POST">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<button class="button-logout" title="Logout" class="win-close"
						type="submit">
						<i class="fa fa-sign-out"></i> Log Out
					</button>
				</form>
			</li>
		</ul>


	</div>
</div>