package nexteon.multichain.oracle.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nexteon.multichain.common.AbstractDBDAO;
import nexteon.multichain.oracle.api.UserRepository;
import nexteon.multichain.oracle.entity.User;


@Repository
@Transactional
public class UserRepositoryImpl extends AbstractDBDAO<User> implements UserRepository {

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean saveUser(User user) {
		boolean status = false;
		try {
			User objErrorCodeMaster = this.insert(user);
			if(objErrorCodeMaster != null){
			status = true;}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;

	}

	@Override
	public User getUserById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUserById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public User getUserByBhamashahId(String username) {
		Map<String,Object> queryParams = new HashMap<>();
		queryParams.put("username", username);
		List<User> list=this.findByCriteria(User.class, queryParams);
		try{
			if(list!=null)
					return list.get(0);
		}
		catch(Exception e){
			
		}
		return null;
	}

	@Override
	public boolean checkCredentials(String password, User user) {

       // String salt = user.getSalt();
      //  String encryptPass = EncryptionUtil.encryptWithSHA(password+salt);
        if(password.equals(user.getPassword())){
            return true;
        }else {
            return false;
        }
	}

}
