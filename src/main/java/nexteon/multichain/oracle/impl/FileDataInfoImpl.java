package nexteon.multichain.oracle.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nexteon.multichain.common.AbstractDBDAO;
import nexteon.multichain.oracle.api.FileDataInfoApi;
import nexteon.multichain.oracle.entity.FileDataInfo;


@Repository
@Transactional
public class FileDataInfoImpl extends AbstractDBDAO<FileDataInfo> implements FileDataInfoApi {

	@Override
	public List<FileDataInfo> getAllFiles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileDataInfo saveFile(FileDataInfo file) {
		  boolean status = false;
		  try {
		   FileDataInfo fObj = this.insert(file);
		   if(fObj != null){
		   return fObj;}
		  } catch (Exception e) {
		   e.printStackTrace();
		  }

		  return null;
		 }

	@Override
	public FileDataInfo getFileById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteFileById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateFile(FileDataInfo file) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FileDataInfo getFileByGuid(String guid) {
		Map<String,Object> queryParams = new HashMap<>();
		queryParams.put("guid", guid);
		List<FileDataInfo> list=this.findByCriteria(FileDataInfo.class, queryParams);
		try{
			if(list!=null)
					return list.get(0);
		}
		catch(Exception e){
			
		}
		return null;
	}

	

}
