package nexteon.multichain.oracle.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import nexteon.multichain.oracle.api.HttpService;


@Repository
@Transactional
public class HttpServiceImpl implements HttpService{

	private static Logger LOGGER = LoggerFactory.getLogger(HttpServiceImpl.class);
	
	@Override
	public String readJsonResponse(String urlString) {
		
		String response = "";
		String output = null;
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				LOGGER.error("Response Code ::" + conn.getResponseCode());
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {

				response = output;
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			LOGGER.error("MalformedURLException :: ",e);

		} catch (IOException e) {

			LOGGER.error("IOException :: ",e);

		}
		catch(Exception e){
			LOGGER.error("MalformedURLException",e);
		}
		//Removed as Logs were getting huge
		//LOGGER.info("Response from HttpClient ::" + response);
		return response;

	}

	
}
