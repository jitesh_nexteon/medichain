package nexteon.multichain.oracle.api;

import java.util.List;

import nexteon.multichain.oracle.entity.FileDataInfo;

public interface FileDataInfoApi {

	public List<FileDataInfo> getAllFiles();

    public FileDataInfo saveFile(FileDataInfo file);

    public FileDataInfo getFileById(Long id);

    public void deleteFileById(Long id);

    public void updateFile(FileDataInfo file);

	public FileDataInfo getFileByGuid(String guid);

}
