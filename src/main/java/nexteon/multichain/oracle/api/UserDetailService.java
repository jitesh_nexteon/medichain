package nexteon.multichain.oracle.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import nexteon.multichain.oracle.entity.User;
import nexteon.multichain.oracle.impl.UserConfig;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailService implements UserDetailsService {


    @Autowired
    private UserRepository userRepos;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = null;
        try {
            user = this.userRepos.getUserByBhamashahId(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserConfig userDetail = null;
        if(user != null) {
            try {
                List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(1);

                userDetail = new UserConfig(username, user.getPassword().toString(), user.getName(), authorities, true, true, true, true);
            } catch (Exception e) {
                System.out.println("Exception :" + e);
            }
        }

        return userDetail;

    }

}
