package nexteon.multichain.oracle.api;

public interface HttpService {
	
	public String readJsonResponse(String urlstring);

}
