package nexteon.multichain.oracle.api;

import java.util.List;
import nexteon.multichain.oracle.entity.User;

/**
 * @author Aashish Kochhar
 */
public interface UserRepository {

	public List<User> getAllUsers();

    public boolean saveUser(User user);

    public User getUserById(Long id);

    public void deleteUserById(Long id);

    public void updateUser(User user);

	public User getUserByBhamashahId(String username);

	public boolean checkCredentials(String password, User userList);

}
