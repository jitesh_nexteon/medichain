package nexteon.multichain.oracle.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import nexteon.multichain.common.BaseDomainModel;
import nexteon.multichain.common.EncryptionUtil;


/**
 * @author Aashish Kochhar
 */

@Entity
@Table(name = "BHAMASHA_USERS")
public class User implements BaseDomainModel {

    
	private static final long serialVersionUID = 2548110185378377752L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId; 

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "Name")
    private String name;

    @Column(name = "role")
    private String role;

    @Column(name = "salt")
    private String salt;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        //this.getNewSalt();
       // this.password = EncryptionUtil.encryptWithSHA(password + this.getSalt());
    	this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSalt() {
        return this.salt;
    }

    private void getNewSalt() {
        this.salt = EncryptionUtil.generateRandomString();
    }

    private void setSalt(String salt) {
        this.salt = salt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
