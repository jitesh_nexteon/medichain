package nexteon.multichain.oracle.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import nexteon.multichain.common.BaseDomainModel;

@Entity
@Table(name = "FileDataInfo")
public class FileDataInfo implements BaseDomainModel {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = -2023586588904568716L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileId; 

    @Column(name = "FileName")
    private String fileName;

    @Column(name = "FilePath")
    private String filePath;

    @Column(name = "GUID")
    private String guid;
    
<<<<<<< HEAD
    @Column(name = "txid")
    private String txid;
=======
    @Column(name = "UserID")
    private String uid;
>>>>>>> origin/master

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}
	
<<<<<<< HEAD
	public String getTxid() {
		return txid;
	}

	public void setTxid(String txid) {
		this.txid = txid;
	}

	public FileDataInfo(String fileName, String filePath, String guid) {
=======
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public FileDataInfo(String fileName, String filePath, String guid, String uid) {
>>>>>>> origin/master
		this.fileName = fileName;
		this.filePath = filePath;
		this.guid = guid;
		this.uid = uid;
	}
	
	public FileDataInfo(){
		
	}

	@Override
	public String toString() {
		return "FileDataInfo [fileId=" + fileId + ", fileName=" + fileName + ", filePath=" + filePath + ", guid=" + guid
				+ ", uid=" + uid + "]";
	}
	
	

}
