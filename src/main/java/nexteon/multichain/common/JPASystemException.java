package nexteon.multichain.common;

/**
 * Exception to cater for Persistence related scenarios.
 * 
 * @author Rakesh.Kumar
 */
public class JPASystemException extends RuntimeException {

	/**
	 * serialVersionUID for this class.
	 */
	private static final long serialVersionUID = -5225468705051383381L;

	/**
	 * Default Constructor.
	 */
	public JPASystemException() {
	}

	/**
	 * Constructor having error code to be propagated further.
	 * 
	 * @param anErrorCode
	 */
	public JPASystemException(String anErrorCode) {
		super(anErrorCode);
	}

	/**
	 * Constructor having error code and the wrapped exception to be propagated
	 * further.
	 * 
	 * @param anErrorCode
	 * @param anException
	 */
	public JPASystemException(String anErrorCode, Exception anException) {
		super(anErrorCode, anException);
	}

	/**
	 * Initializes the super with the exception.
	 * 
	 * @param exception
	 */
	public JPASystemException(RuntimeException exception) {
		super(exception);
	}

	public JPASystemException(Throwable throwable) {
		super(throwable);
	}

}