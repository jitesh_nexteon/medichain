package nexteon.multichain.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.persistence.exceptions.EclipseLinkException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class AbstractDBDAO<T extends BaseDomainModel> implements GenericDBDAO<T> {

	/**
	 * logger for AbstractDBDAO class.
	 */
	private static final Log logger = LogFactory.getLog(AbstractDBDAO.class);

	/**
	 * EntityManager reference injected by Server Container if deployed in an
	 * application server or by Spring if running in stand alone mode and Spring
	 * is taking care of creation of EntityManager.
	 */
	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * Inserts a transient instance of the Entity to the designated table, after
	 * the insertion is successful the instance becomes a managed instance until
	 * the entity manager is closed, if the caller then fires the find operation
	 * then the same Entity instance is returned.
	 *
	 * This method should be run in a Transaction therefore expects a
	 * transaction to be initiated before it reaches here. If no transaction
	 * exists then an exception is thrown back to the caller.
	 *
	 * The annotation MANDATORY forces that the caller should call this
	 * operation in a transaction.
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 *
	 * @param transientObj
	 *            the Entity instance that is to be inserted.
	 * @return T persistentEntity
	 *
	 */
	@Transactional(propagation = Propagation.MANDATORY)
	@Override
	public T insert(T transientObj) {
		System.out.println("Entity to be inserted :" + transientObj);
		try {
			this.entityManager.persist(transientObj);
			return transientObj;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while inserting :", ex);
			throw new JPASystemException("Update failed:", ex);
		}
	}

	/**
	 * Merge the state of the given entity into the current instance available
	 * in persistence context. Entity manager first fetch the instance from
	 * database and then merge the contents from the instance in memory into the
	 * instance retrieved from database and then commits.
	 *
	 * This method should be run in a Transaction therefore expects a
	 * transaction to be initiated before it reaches here. If no transaction
	 * exists then an exception is thrown back to the caller.
	 *
	 * The annotation MANDATORY forces that the caller should call this
	 * operation in a transaction.
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param persistentObj
	 *            the persistent object
	 * @return T the managed instance that the state was merged to
	 * @throws JPASystemException
	 */
	@Transactional(propagation = Propagation.MANDATORY)
	@Override
	public T update(T persistentObj) {
		logger.info("Entity to be updated :" + persistentObj);
		try {
			return this.entityManager.merge(persistentObj);
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while updating :", ex);
			throw new JPASystemException("Update failed:", ex);
		}
	}

	/**
	 * This method finds entity or list of entity by a criteria. A criteria map
	 * is the input to the method where key name will be the field name in the
	 * Entity class "as-is" and the value will be of the field type. Consider
	 * the example of an entity Employee where fields are: name of type String
	 * and employeeId as Integer. Usage of this method in finding out the
	 * Employee entity from DB will be.
	 *
	 * <br>
	 *
	 * Usage:
	 *
	 * <br>
	 *
	 * <code>
	 * 	Map<String, Object> queryParams = new HashMap<String, Object>();<br>
	 * 	queryParams.put("name", "Peter");<br>
	 * 	queryParams.put("employeeId", 78470);<br>
	 *
	 * 	dao.findByCriteria(Employee.class, queryParams);<br>
	 *
	 * 	This method then creates the dynamic JPA query as:<br>
	 * 	select e from Employee e where e.name = :name and e.employeeId = :78470
	 * </code>
	 *
	 * <br>
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param criteriaClass
	 *            EntityName as a Class
	 * @param queryParams
	 *            map as criteria mappings.
	 * @return List of entity
	 * @throws JPASystemException
	 */
	@Override
	public List<T> findByCriteria(Class<T> criteriaClass, Map<String, Object> queryParams) {
		logger.info("queryParams : " + queryParams);
		try {
			CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(criteriaClass);
			Root<T> persistentObj = cq.from(criteriaClass);
			Set<Entry<String, Object>> entrySet = queryParams.entrySet();
			List<Predicate> predicates = new ArrayList<>();
			for (Entry<String, Object> entry : entrySet) {
				predicates.add(cb.equal(persistentObj.get(entry.getKey()), entry.getValue()));
			}
			cq.select(persistentObj).where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			TypedQuery<T> tquery = this.entityManager.createQuery(cq);
			return tquery.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			logger.error("Exception while finding :", ex);
			throw new JPASystemException("Find operation failed:", ex);
		}

	}

	/**
	 * This method fetches all the rows from the DB for the given criteria class
	 * by firing the named query. This method also expects the positional
	 * parameter to be passed at runtime to the query if there are any. Named
	 * Query will be residing in the orm.xml file, a named query can be in JPQL
	 * or in native SQL.
	 *
	 * <br>
	 *
	 * Usage:
	 *
	 * <br>
	 *
	 * <code>
	 * 	Object[] queryParams  = {"Peter"};<br>
	 * 	dao.findByNamedQuery(Employee.class, "findByName", queryParams);<br>
	 *
	 * 	Named Query "findByName" is declared as :<br>
	 * 	select e from Employee e where e.name = ?1<br>
	 *
	 * 	This operation will then apply the query parameter and the resultant query will be.<br>
	 * 	select e from Employee e where e.name = 'Peter'
	 * </code>
	 *
	 * <br>
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param criteriaClass
	 *            the criteria Entity class which is to be fetched.
	 * @param namedQuery
	 *            the query JPQL or native SQL format named query.
	 * @param queryParams
	 *            position parameter array
	 * @return List of all the rows returned from the query.
	 * @throws JPASystemException
	 *
	 */
	@Override
	public List<T> findByNamedQuery(Class<T> criteriaClass, String namedQuery, Object... queryParams) {
		logger.info("NamedQuery fired : " + namedQuery);
		try {
			TypedQuery<T> tquery = this.entityManager.createNamedQuery(namedQuery, criteriaClass);
			this.setQueryParameters(tquery, queryParams);
			return tquery.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while finding :", ex);
			throw new JPASystemException("NamedQuery failed with exception:", ex);
		}

	}

	@Override
	public List<T> selectByQuery(String query) {
		try {
			TypedQuery<T> tquery = (TypedQuery<T>) this.entityManager.createQuery(query);
			List<T> results = tquery.getResultList();
			return results;
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			logger.error("Exception while updating :", ex);
			throw new JPASystemException("Insertion failed:", ex);
		}
	}
	
	
	@Override
	public  List<T> selectByQuery(String query ,int startIndex, int limit) {
		try  {
			logger.info("Inside Select By Query");			
			TypedQuery<T> q = (TypedQuery<T>) this.entityManager.createQuery(query);
			q.setFirstResult(startIndex);
			q.setMaxResults(limit);
			List<T> results = q.getResultList();
			return results;
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			logger.error("Exception while updating :", ex);
			throw new JPASystemException("Insertion failed:", ex);
		}
	}


	/**
	 * This method fetches the result as a scalar value from the DB for the
	 * given scalarResultClass by firing the named query. This method also
	 * expects the positional parameter to be passed at runtime to the query if
	 * there are any. Named Query will be residing in the orm.xml file, a named
	 * query can be in JPQL or in native SQL format.
	 *
	 * <br>
	 *
	 * Usage:
	 *
	 * <br>
	 *
	 * <code>
	 * 	Object[] queryParams  = {"Peter"};<br>
	 * 	dao.findScalarResultByNamedQuery(String.class, "findByName", queryParams);<br>
	 *
	 * 	Named Query "findByName" is declared as : <br>
	 * 	select e.employeeId from Employee e where e.name = ?1<br>
	 *
	 * 	This operation will then apply the query parameter and the resultant query will be.<br>
	 * 	select e.employeeId from Employee e where e.name = 'Peter'
	 * </code>
	 *
	 * <br>
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param scalarResultClass
	 *            the class of the scalar value which is to be fetched.
	 * @param namedQuery
	 *            the query JPQL or native SQL format named query.
	 * @param queryParams
	 *            position parameter array
	 * @return E as a scalar value returned from the query.
	 * @throws JPASystemException
	 *
	 */
	public <E> E getScalarResultByNamedQuery(Class<E> scalarResultClass, String namedQuery, Object... queryParams) {
		logger.info("NamedQuery fired : " + namedQuery);
		try {
			TypedQuery<E> tquery = this.entityManager.createNamedQuery(namedQuery, scalarResultClass);
			this.setQueryParameters(tquery, queryParams);
			return tquery.getSingleResult();
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while fetching scalar result:", ex);
			throw new JPASystemException("Fetching scalar result failed!", ex);
		}
	}

	/**
	 * This method fetches all the rows from DB for a given Entity class.
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param entityClass
	 *            the entity class
	 * @return List of all the rows returned from the query.
	 * @throws JPASystemException
	 */
	@Override
	public List<T> findAll(Class<T> entityClass) {
		logger.info("findAll invoked for Entity : " + entityClass.getName());
		try {
			CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(entityClass);
			Root<T> persistentObj = cq.from(entityClass);
			cq.select(persistentObj);
			TypedQuery<T> tq = this.entityManager.createQuery(cq);
			return tq.getResultList();
		} catch (PersistenceException | EclipseLinkException | IllegalStateException | IllegalArgumentException ex) {
			logger.error("Exception while finding records :", ex);
			throw new JPASystemException("Find All failed:", ex);
		}

	}

	/**
	 * This method deletes the given entity from Database.
	 *
	 * The way this method works is that The EntityManager will first search the
	 * given entity instance in its context if found then it will be removed, if
	 * the given entity instance is not found then the current state of the
	 * instance is merged to the instance available in the database and then the
	 * remove operation will be fired. This is required because
	 * EntityManager.remove will throw an exception if the entity instance is a
	 * detached one.
	 *
	 * This method should be run in a Transaction therefore expects a
	 * transaction to be initiated before it reaches here. If no transaction
	 * exists then an exception is thrown back to the caller.
	 *
	 * The annotation MANDATORY forces that the caller should call this
	 * operation in a transaction.
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param entityInstance
	 *            which is to be removed from DB
	 * @throws JPASystemException
	 */
	@Transactional(propagation = Propagation.MANDATORY)
	@Override
	public void delete(T entityInstance) {
		logger.info("Entity to be deleted :" + entityInstance);
		try {
			this.entityManager.remove(this.entityManager.contains(entityInstance) ? entityInstance
					: this.entityManager.merge(entityInstance));
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while deleting :" + ex);
			throw new JPASystemException("Deletion failed:", ex);
		}

	}

	/**
	 * This method removes the Entity row from the DB for the given entityClass
	 * by firing the named query defined in orm.xml file. This method also
	 * expects the positional parameter to be passed at runtime to the query if
	 * query demands them. A named query can be in JPQL or in native SQL format.
	 * This method can be used as the way depicted below.
	 *
	 * <br>
	 *
	 * Usage:
	 *
	 * <br>
	 *
	 * <code>
	 * 	Object[] queryParams  = {78470};<br>
	 * 	dao.deleteByNamedQuery(Employee.class, "Employee.deleteById", queryParams);<br>
	 *
	 * 	Named Query "Employee.deleteById" is declared as :<br>
	 * 	delete from Employee e where e.employeeId = ?1<br>
	 *
	 * 	This operation will then apply the query parameter and the resultant query will be.<br>
	 * 	delete from Employee e where e.employeeId = 78470
	 * </code>
	 *
	 * <br>
	 *
	 * This method should be run in a Transaction therefore expects a
	 * transaction to be initiated before it reaches here. If no transaction
	 * exists then an exception is thrown back to the caller.
	 *
	 * The annotation MANDATORY forces that the caller should call this
	 * operation in a transaction.
	 *
	 * This operation may throw a JPASystemException which is an unchecked
	 * exception. Caller should make sure that it is handled gracefully.
	 *
	 * @param entityClass
	 *            the class of the Entity
	 * @param namedQuery
	 *            the query JPQL or native SQL format named query.
	 * @param queryParams
	 *            position parameter array
	 * @return count of the rows removed by the query.
	 * @throws JPASystemException
	 *
	 */
	@Transactional(propagation = Propagation.MANDATORY)
	@Override
	public int deleteByNamedQuery(Class<T> entityClass, String namedQuery, Object... queryParams) {
		logger.info("NamedQuery fired:" + namedQuery);
		try {
			TypedQuery<T> tquery = this.entityManager.createNamedQuery(namedQuery, entityClass);
			this.setQueryParameters(tquery, queryParams);
			int rowsDeleted = tquery.executeUpdate();
			logger.info("Rows deleted :" + rowsDeleted);
			return rowsDeleted;
		} catch (PersistenceException | EclipseLinkException | IllegalArgumentException ex) {
			logger.error("Exception while deleting :" + ex);
			throw new JPASystemException("NamedQuery Deletion failed:", ex);
		}

	}

	/**
	 * This method sets the positional query parameters passed by the caller.
	 *
	 * @param tquery
	 * @param queryParams
	 */
	private void setQueryParameters(TypedQuery<?> tquery, Object... queryParams) {
		if (queryParams != null && queryParams.length > 0) {
			for (int i = 0; i < queryParams.length; i++) {
				Object queryParam = queryParams[i];
				logger.info("queryParam :" + queryParam);
				// Positional parameters always starts with 1.
				tquery.setParameter(i + 1, queryParam);
			}
		}
	}

}
