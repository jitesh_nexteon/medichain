package nexteon.multichain.common;
import java.util.List;
import java.util.Map;


public interface GenericDBDAO<T extends BaseDomainModel> {

    /**
     * Inserts a transient instance of the Entity to the designated table, after
     * the insertion is successful the instance becomes a managed until the
     * entity manager is closed, if the caller then fires the find operation
     * then the same Entity instance is returned.
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param transientObj
     *            the Entity instance that is to be inserted.
     * @return T persistentEntity
     * @throws JPASystemException
     */
    T insert(T transientObj);

    /**
     * Merge the state of the given entity into the current instance available
     * in persistence context. Entity manager first fetch the instance from
     * database and then merge the contents from the instance in memory into the
     * instance retrieved from database and then commits.
     *
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param persistentObj
     *            the persistent object
     * @return T the managed instance that the state was merged to
     * @throws JPASystemException
     */
    T update(T persistentObj);

    /**
     * This method finds entity or list of entity by a criteria. A criteria map
     * is the input to the method where key name will be the field name in the
     * Entity class "as-is" and the value will be of the field type. Consider
     * the example of an entity Employee where fields are: name of type String
     * and employeeId as Integer. Usage of this method in finding out the
     * Employee entity from DB will be.
     *
     * <br>
     *
     * Usage:
     *
     * <br>
     *
     * <code>
     * 	Map<String, Object> queryParams = new HashMap<String, Object>();
     * 	queryParams.put("name", "Peter");
     * 	queryParams.put("employeeId", 78470);
     *
     * 	dao.findByCriteria(Employee.class, queryParams);
     *
     * 	This method then creates the dynamic JPA query as:
     * 	select e from Employee e where e.name = :name and e.employeeId = :78470
     * </code>
     *
     * <br>
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param criteriaClass
     *            EntityName as a Class
     * @param queryParams
     *            map as criteria mappings.
     * @return List of entity
     * @throws JPASystemException
     */
    List<T> findByCriteria(Class<T> criteriaClass,
                           Map<String, Object> queryParams);

    /**
     * This method fetches all the rows from the DB for the given criteria class
     * by firing the named query. This method also expects the positional
     * parameter to be passed at runtime to the query if there are any. Named
     * Query will be residing in the orm.xml file, a named query can be in JPQL
     * or in native SQL.
     *
     * <br>
     *
     * Usage:
     *
     * <br>
     *
     * <code>
     * 	Object[] queryParams  = {"Peter"};
     * 	dao.findByNamedQuery(Employee.class, "findByName", queryParams);
     *
     * 	Named Query "findByName" is declared as :
     * 	select e from Employee e where e.name = ?1
     *
     * 	This operation will then apply the query parameter and the resultant query will be.
     * 	select e from Employee e where e.name = 'Peter'
     * </code>
     *
     * <br>
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param criteriaClass
     *            the criteria Entity class which is to be fetched.
     * @param namedQuery
     *            the query JPQL or native SQL format named query.
     * @param queryParams
     *            position parameter array
     * @return List of all the rows returned from the query.
     * @throws JPASystemException
     *
     */
    List<T> findByNamedQuery(Class<T> criteriaClass, String namedQuery,
                             Object... queryParams);
    
   List<T> selectByQuery(String Query);

    /**
     * This method fetches the result as a scalar value from the DB for the
     * given criteria class by firing the named query. This method also expects
     * the positional parameter to be passed at runtime to the query if there
     * are any. Named Query will be residing in the orm.xml file, a named query
     * can be in JPQL or in native SQL.
     *
     * <br>
     *
     * Usage:
     *
     * <br>
     *
     * <code>
     * 	Object[] queryParams  = {"Peter"};
     * 	dao.findScalarResultByNamedQuery(String.class, "findByName", queryParams);
     *
     * 	Named Query "findByName" is declared as :
     * 	select e.employeeId from Employee e where e.name = ?1
     *
     * 	This operation will then apply the query parameter and the resultant query will be.
     * 	select e.employeeId from Employee e where e.name = 'Peter'
     * </code>
     *
     * <br>
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param scalarResultClass
     *            the class of the scalar value which is to be fetched.
     * @param namedQuery
     *            the query JPQL or native SQL format named query.
     * @param queryParams
     *            position parameter array
     * @return E as a scalar value returned from the query.
     * @throws JPASystemException
     *
     */
    <E> E getScalarResultByNamedQuery(Class<E> scalarResultClass,
                                      String namedQuery, Object... queryParams);
    
    

    /**
     * This method fetches all the rows from DB for a given Entity class.
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param entityClass
     *            the entity class
     * @return List of all the rows returned from the query.
     * @throws JPASystemException
     */
    List<T> findAll(Class<T> entityClass);

    /**
     * This method deletes the given entity from Database.
     *
     * The way this method works is that The EntityManager will first search the
     * given entity instance in its context if found then it will be removed, if
     * the given entity instance is not found then the current state of the
     * instance is merged to the instance available in the database and then the
     * remove operation will be fired. This is required because
     * EntityManager.remove will throw an exception if the entity instance is a
     * detached one.
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param entityInstance
     *            which is to be removed from DB
     * @throws JPASystemException
     */
    void delete(T entityInstance);

    /**
     * This method removes the Entity row from the DB for the given entityClass
     * by firing the named query defined in orm.xml file. This method also
     * expects the positional parameter to be passed at runtime to the query if
     * query demands them. A named query can be in JPQL or in native SQL format.
     * This method can be used as the way depicted below.
     *
     * <br>
     *
     * Usage:
     *
     * <br>
     *
     * <code>
     * 	Object[] queryParams  = {78470};
     * 	dao.deleteByNamedQuery(Employee.class, "Employee.deleteById", queryParams);
     *
     * 	Named Query "Employee.deleteById" is declared as :
     * 	delete from Employee e where e.employeeId = ?1
     *
     * 	This operation will then apply the query parameter and the resultant query will be.
     * 	delete from Employee e where e.employeeId = 78470
     * </code>
     *
     * <br>
     * This method should be run in a Transaction therefore expects a
     * transaction to be initiated before it reaches here. If no transaction
     * exists then an exception is thrown back to the caller.
     *
     * The annotation MANDATORY forces that the caller should call this
     * operation in a transaction.
     *
     * This operation may throw a JPASystemException which is an unchecked
     * exception. Caller should make sure that it is handled gracefully.
     *
     * @param entityClass
     *            the class of the Entity
     * @param namedQuery
     *            the query JPQL or native SQL format named query.
     * @param queryParams
     *            position parameter array
     * @return count of the rows removed by the query.
     * @throws JPASystemException
     *
     */
    int deleteByNamedQuery(Class<T> entityClass, String namedQuery,
                           Object... queryParams);

	List<T> selectByQuery(String query, int startIndex, int limit);
    
    
  

}
