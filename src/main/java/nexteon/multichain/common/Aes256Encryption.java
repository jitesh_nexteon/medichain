package nexteon.multichain.common;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class Aes256Encryption {
	private SecretKeySpec secretKey;
	private IvParameterSpec ivParameterSpec;
	
	/**
	 * Encrypt the string
	 * 
	 * @param toBeEncryptString
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public String encrypt(String toBeEncryptString, String key) throws Exception {
		if (toBeEncryptString == null) {
			throw new IllegalArgumentException("To be encrypt string must not be null");
		}
		try {
			setKey(key);
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(1, this.secretKey, this.ivParameterSpec);
			return DatatypeConverter.printBase64Binary(cipher.doFinal(toBeEncryptString.getBytes("UTF-8")));
		} catch (Exception ex) {
			throw new Exception("Error in encrypting the values using AES256", ex);
		}
	}

	/**
	 * Decrypt the string
	 * 
	 * @param toBeDecryptString
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public String decrypt(String toBeDecryptString, String key) throws Exception {
		if (toBeDecryptString == null) {
			throw new IllegalArgumentException("To be decrypt string must not be null");
		}
		try {
			setKey(key);
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(2, this.secretKey, this.ivParameterSpec);

			byte[] original = cipher.doFinal(DatatypeConverter.parseBase64Binary(toBeDecryptString));

			return new String(original);
		} catch (Exception ex) {
			throw new Exception("Error in decrypting the values using AES256", ex);
		}
	}

	private void setKey(String key) throws Exception {
		MessageDigest sha = null;
		try {
			byte[] byteData = key.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-256");
			byteData = sha.digest(byteData);
			byteData = Arrays.copyOf(byteData, 16);
			this.secretKey = new SecretKeySpec(byteData, "AES");
			this.ivParameterSpec = new IvParameterSpec(byteData);
		} catch (Exception ex) {
			throw new Exception("Key could not be set using AES256", ex);
		}
	}
}

