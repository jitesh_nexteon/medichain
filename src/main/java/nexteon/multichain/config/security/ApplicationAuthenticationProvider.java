package nexteon.multichain.config.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import nexteon.multichain.oracle.api.UserDetailService;



@Component
public class ApplicationAuthenticationProvider implements AuthenticationProvider {
	

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationAuthenticationProvider.class);

    
    @Autowired
    UserDetailService userService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

     UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
        String username = String.valueOf(auth.getPrincipal());
        String password = String.valueOf(auth.getCredentials());
        UserDetails userDetails = null;
    
        try {
            userDetails = this.userService.loadUserByUsername(username);
        }
        catch(UsernameNotFoundException ex){
            System.out.println("Cannot find user with this username: "+ username);
        }

        if(userDetails != null) {
            if (userDetails.getUsername().equals(username) && userDetails.getPassword().equals(password)) {

                Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
                Authentication authenticated = new UsernamePasswordAuthenticationToken(username, password, authorities);
                return authenticated;
            }
        }

        return null;
    }
   
    
    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    } 
}