package nexteon.multichain.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private ApplicationAuthenticationProvider applicationAuthenticationProvider;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(new ApplicationAuthenticationProvider());
        auth.inMemoryAuthentication().withUser("admin").password("admin").roles("ADMIN");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()

                .antMatchers("/login","/register","/static*//**//**").permitAll()
                .and()
                .formLogin()
                .loginPage("/login").and().logout().logoutUrl("/logout").logoutSuccessUrl("/");
        http.csrf().ignoringAntMatchers("/FileUpload","/addpromo","/openwallet*//**//**","/paybill*//**//**","/applycode");
    }


    @Override
    protected  void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
        authenticationManagerBuilder.authenticationProvider(applicationAuthenticationProvider);

    }


}