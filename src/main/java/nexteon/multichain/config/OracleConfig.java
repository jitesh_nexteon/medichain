package nexteon.multichain.config;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Aashish Kochhar 
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 */

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.nexteon.doit.chain" })
public class OracleConfig {

    private static final String Persistence_Unit_name = "DASHBOARD";
    private static EntityManagerFactory factory;

    @Bean
    public EntityManagerFactory factoryEntity() {
        factory = Persistence.createEntityManagerFactory(Persistence_Unit_name);
        System.out.println("factory ::" +factory);
        EntityManager em = factory.createEntityManager();
        return factory;

    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        factory = this.factoryEntity();
        jpaTransactionManager.setEntityManagerFactory(factory);
        return jpaTransactionManager;
    }
}
