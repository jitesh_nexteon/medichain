package nexteon.multichain.medicalRecord;

import java.io.*;

import nexteon.multichain.command.builders.QueryBuilderBlock;

public class QueryBlock extends QueryBuilderBlock {

	public static String execCommand(String CHAIN, String command, String... parameters) {
		Runtime run = Runtime.getRuntime();
		Process pr = null;
		String result = "";
		try {
			if (parameters.length > 0) {
				String params = "";
				for (String parameter : parameters) {
					params = params.concat(parameter + " ");
				}
				pr = run.exec("C:\\medchain\\exe\\multichain-cli " + CHAIN + " "+ command.toString().toLowerCase() + " " + params);
				} else {
				pr = run.exec("C:\\medchain\\exe\\multichain-cli " + CHAIN + " "+ command.toString().toLowerCase());
			}

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				result = result.concat(s + "\n");
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String execDaemonCommand(String CHAIN) {
		Process pr = null;
		;
		String result = "";
		try {
			if (!CHAIN.equals("")) {
				ProcessBuilder builder = new ProcessBuilder("E:\\multichain-data\\multichaind " + CHAIN + "-daemon");
				builder.redirectErrorStream(true);
				pr = builder.start();
			}
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				result = result.concat(s + "\n");
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
