package nexteon.multichain.controller;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import nexteon.multichain.medicalRecord.QueryBlock;
import nexteon.multichain.oracle.entity.FileDataInfo;
import nexteon.multichain.oracle.entity.User;

@Controller
public class DisplayController {

	@RequestMapping(value="/result",method = RequestMethod.GET)
	public ModelAndView displayHome(){
		String chain = "myDemoChain";
		ModelAndView model = new ModelAndView("result");
		try
		{
			String user= (String)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String jsonData = QueryBlock.execCommand( chain , "liststreams " , user);
			JSONArray array = new JSONArray(jsonData);
			JSONObject userData = new JSONObject(array.get(0).toString());
			JSONObject userDetails = new JSONObject(userData.get("details").toString());
			String name = userDetails.get("name").toString();
			String bhamashahId = userDetails.get("loginid").toString();
			
			User u = new User();
			FileDataInfo[] fi = null;
			u.setName(name);
			u.setUsername(bhamashahId);
			
			//File Data reading
			String fileData = QueryBlock.execCommand( chain , "liststreamitems " , user);
			JSONArray fileArray = new JSONArray(fileData);
			for(int i=0;i<fileArray.length();i++)
			{
				JSONObject fileDetails = new JSONObject(fileArray.get(i).toString());
				String key = fileDetails.get("key").toString();
				String value = fileDetails.get("value").toString();
				String keyData = QueryBlock.execCommand( chain , "liststreams " , key);
				JSONArray fa = new JSONArray(keyData);
				JSONObject fd = new JSONObject(fa.get(0).toString());
				JSONObject fparams = new JSONObject(fd.get("details").toString());
				String txn = fd.get("createtxid").toString();
				String uploadedBy = fparams.get("uploadBy").toString();
				String documentName = fparams.get("documentName").toString();
				String GUID = fparams.get("GUID").toString();
				fi=new FileDataInfo[fileArray.length()];
				fi[i].setFileName(uploadedBy);
				fi[i].setFilePath(documentName);
				fi[i].setGuid(GUID);
				fi[i].setTxid(txn);;
			}
			model.addObject("fileInfo",fi);
			model.addObject("userData", u);
			System.out.println(jsonData);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return model;
	}
	
}
