package nexteon.multichain.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import nexteon.multichain.medicalRecord.QueryBlock;
import nexteon.multichain.oracle.api.FileDataInfoApi;
import nexteon.multichain.oracle.api.UserRepository;
import nexteon.multichain.oracle.entity.FileDataInfo;
import nexteon.multichain.oracle.entity.User;

@Controller
public class RecordController {
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	FileDataInfoApi fileDataInfo;
	
	private String randomUUIDString;

	public Logger logger=Logger.getLogger(RecordController.class);
	
	String filePath = "";
	
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String displayHome(){
		return "redirect:/login";
	}

    @RequestMapping(value="/login",method = RequestMethod.GET)
    public String login(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!(auth instanceof AnonymousAuthenticationToken))
        {
            return "upload";
        }
        return "login";
    }
    
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ModelAndView loginUser(HttpServletRequest request){

        ModelAndView model = new ModelAndView("login");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean status = false;
        User userList = this.userRepo.getUserByBhamashahId(username);
        if(userList != null) {
             status = this.userRepo.checkCredentials(password, userList);
        }
        if(status == true){
            return new ModelAndView("upload");
        }
        else{
            model.addObject("error","Invalid Username or Password!");
        }
        return model;
    }
	 
     @RequestMapping(value="/FileUpload",method = RequestMethod.POST)
	    public ModelAndView upload( @RequestParam MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException{
    	 ServletContext context=request.getServletContext();
         String rootPath = "C:\\medchain\\upload";
         logger.info("Path = " + rootPath);
         MultipartFile m = file;
         logger.info(m.toString());
         File dir = new File(rootPath+"\\"+m.getOriginalFilename());
         filePath = dir.getAbsolutePath();
         if (!m.isEmpty()) {
             try {
                 byte[] bytes = m.getBytes();
                 BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(dir));
                 stream.write(bytes);
                 stream.close();
                 logger.info("Image uploaded");
               } catch (Exception ex) {
                 logger.info(ex.getMessage());
                 return null;
             }    	 
    	 
     }
         User user = this.userRepo.getUserByBhamashahId(request.getUserPrincipal().getName());
         FileDataInfo fileObj = null;
         //ModelAndView model=new ModelAndView();
         try{
        	 UUID uuid = UUID.randomUUID();
	         String randomUUIDString = uuid.toString();
	         
	         fileObj  = new FileDataInfo(m.getOriginalFilename(), dir.toString(), randomUUIDString, user.getUsername());
	          if(fileObj != null){
	           fileObj= this.fileDataInfo.saveFile(fileObj);
	          }
         }
         catch(Exception e){
        	 e.printStackTrace();
         }
         
         QueryBlock queryBlc = new QueryBlock();
         try{
          User usr =new User();
          //usr = this.userRepo.getUserById(uid);
          String param = "\"{\\\"uploadBy\\\":\\\""+fileObj.getFileId()+"\\\",\\\"documentName\\\":\\\""+m.getOriginalFilename()+"\\\",\\\"filePath\\\":\\\""+fileObj.getFilePath()+"\\\",\\\"GUID\\\":\\\""+randomUUIDString+"\\\"}\"";
         // QueryBlock.execCommand("chain1", "create stream "+fileObj.getFileId()+" true", param);
          String res = queryBlc.execCommand("chain1", "create stream "+fileObj.getFileId()+" true", param);
          if(res != null && !res.contains("error") && fileObj.getFileId() != null)
	    	{
        	  	String hashOfFile = hashKey(fileObj);
	    		String publish = QueryBlock.execCommand("chain1", "publish "+user.getUserId() +" "+ fileObj.getFileId() +" "+ hashOfFile );
	    		if(publish != null && !publish.equals("")){
	    			System.out.println("publish" + publish);
	    		}
	    	}
         }
         catch(Exception e){
          e.printStackTrace();
         }
         
         return new ModelAndView("result");
     }
     
     @RequestMapping(value="/profile",method = RequestMethod.GET)
     public String viewDocuments(){
     	Authentication auth = SecurityContextHolder.getContext().getAuthentication();

         if (!(auth instanceof AnonymousAuthenticationToken))
         {
             return "result";
         }
         return "login";
     }
     

     @RequestMapping(value="/addblock",method = RequestMethod.GET)
     public void addBlock(HttpServletRequest req, HttpServletResponse res){
    	 ServletContext context = req.getServletContext();
    	 String rootPath = context.getRealPath("/");
    	
    	 	QueryBlock queryBlock = new QueryBlock();
    	 	try {
    	 		res.setContentType("text/html");
    	 		String output = queryBlock.execCommand("myDemoChain","issue", "2a8f9214bf854c7a5e28177929ec620bd70c4a962e1475ff212f428fac5693d3","\""+filePath+"\"","1","1");
    	 		//String output = queryBlock.createChain("myDemoChain");
				//res.getWriter().print(output+"<br/>"+queryBlock.getBlockCount());
			} catch (Exception e) {
				System.out.println(e);
			}
    	 
    	 
     }
     
     @RequestMapping(value="/register",method = RequestMethod.GET)
     public String register(){
     	Authentication auth = SecurityContextHolder.getContext().getAuthentication();

         if (!(auth instanceof AnonymousAuthenticationToken))
         {
             return "upload";
         }
         return "register_new";
     }
     
     @RequestMapping(value="/register", method= RequestMethod.POST)
     public String registerUser(HttpServletRequest request){

     	ModelAndView model = new ModelAndView("login");
     	String name = request.getParameter("name");
 		String fName = request.getParameter("family");
 		String pass = request.getParameter("pass");
 		String confirm = request.getParameter("cpass");
 		if(!(fName.isEmpty() || fName.equals("")) && pass.equals(confirm) ){
 			try {
 				
 		    	User user=new User();
 		    	user.setName(name);
 		    	user.setUsername(fName);
 		    	user.setPassword(confirm);
 		    	user.setRole("ROLE_USER");
 		    	boolean status = this.userRepo.saveUser(user);
 		    	String userData = "\"{\\\"name\\\":\\\""+name+"\\\",\\\"loginId\\\":\\\""+fName+"\\\",\\\"password\\\":\\\""+confirm+"\\\"}\"";
 		    	String result = QueryBlock.execCommand("chain1", "create stream "+fName+" true "+ userData);
 		    	if(result != null && !result.contains("error") && status)
 		    	{
 		    		String subscribe = QueryBlock.execCommand("chain1", "subscribe "+fName);
 		    		model.addObject("msgRes", "Successfully Registered! Please login Now.");
 		    		return "upload";
 		    		
 		    	}else{
 		    		model.addObject("msgRes", "Unable to register User");
 		    		return "redirect:/login";
 		    	}
	
 			}
 			catch (Exception e) {
 				model.addObject("msgRes", "Error!Check your bhamashah id and try again.");
 			}
 		}
 		else
 		{
 			model.addObject("msgRes", "Username and Password is incorrect.");
 		}
 		
 		return "redirect:/login";
     }
     
     
     
 	private String hashKey(FileDataInfo object) {
 		MessageDigest md;
 		String hashKey = null;
 		try {
 			md = MessageDigest.getInstance("MD5");
 		
 		//LOGGER.info(auaXmlRequest.toString());
 		md.update(object.toString().getBytes());
 		byte[] bytes = md.digest();
 		StringBuilder sb = new StringBuilder();
 		for (int i = 0; i < bytes.length; i++) {
 			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
 		}
 		hashKey = sb.toString();
 		} catch (NoSuchAlgorithmException e) {
 			System.out.println("NoSuchAlgorithmException :" + e);
 		}
 		return hashKey;
 	}

     
}
